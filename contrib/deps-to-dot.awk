#!/usr/bin/awk -f

BEGIN {
  print "digraph deps {"
  print "rankdir=\"LR\";"
}

{
  print "\"" $1 "\" -> \"" $2 "\""
}

END {
  print "}"
}

