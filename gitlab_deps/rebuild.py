import logging
import time


def rebuild(gl, project_path, branch_name, wait=False):
    """Trigger a rebuild of a project."""
    project = gl.projects.get(project_path)
    if not project:
        return None

    pipeline = project.pipelines.create({'ref': branch_name})
    logging.info('started pipeline %s', pipeline.web_url)
    if wait:
        while pipeline.finished_at is None:
            pipeline.refresh()
            time.sleep(3)
    return pipeline


def rebuild_deps(gl, project_deps, project_path, branch_name, dry_run,
                 wait_and_recurse):
    stack = project_deps.get((project_path, branch_name), [])
    while stack:
        path, branch = stack.pop(0)
        logging.info('rebuilding %s:%s', path, branch)
        if not dry_run:
            rebuild(gl, path, branch, wait_and_recurse)
        if wait_and_recurse:
            stack.extend(project_deps.get((path, branch), []))
